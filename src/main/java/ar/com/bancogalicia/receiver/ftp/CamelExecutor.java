package ar.com.bancogalicia.receiver.ftp;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;


/**
 * The CamelContext initializer.
 * The CamelContext represents a single Camel routing rulebase. You use the CamelContext in a similar way to the Spring ApplicationContext.
 * 
 */
public class CamelExecutor {
	
	private int waitForFile = 60000;
	private CamelContext camelCtx;
	
/**
 * The init method to retrieve the RouteBuilder and start the CamelContext for the job.
 */
	public void init() throws Exception {
		camelCtx = new DefaultCamelContext();
		try {
			camelCtx.addRoutes(new ConsumerRouteBuilder());
			if (camelCtx.getStatus().isStopped()) {
				camelCtx.start();
				System.out.println("CamelContext initialized, ready to consume file.");
				String source = ConsumerRouteBuilder.getPropertiesMap().get("FTP_HOST")+":"+ConsumerRouteBuilder.getPropertiesMap().get("FTP_DIR");
				String dest = ConsumerRouteBuilder.getPropertiesMap().get("DEST_FOLDER");
				System.out.println("[FROM] Ftp Source "+source);
				System.out.println("[TO] Local Destination "+dest);
				System.out.println(">>> PLEASE upload a file to the FTP server within "+(waitForFile/1000)+" seconds and wait for consuming... ");
				
				Thread.sleep(waitForFile);
			}
		} catch (Exception e) {
			System.out.println("Unable to initialize CamelContext: " + e);
			return;
		}
	}
}
