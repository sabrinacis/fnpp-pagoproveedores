/**
 * This file is part of GiuseppeUrso-EU Software.
 * 
 * GiuseppeUrso-EU Software is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *             
 * GiuseppeUrso-EU Software Software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GiuseppeUrso-EU Software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package ar.com.bancogalicia.receiver.ftp;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ConsumerRouteBuilder extends RouteBuilder {
 
	private static ResourceBundle rb = ResourceBundle.getBundle("application");
	
	final static String DEST_FOLDER = rb.getString("consumer.destFolder").trim();
	
	final static String FTP_HOST = rb.getString("consumer.ftp.host").trim();
	final static String FTP_PORT = rb.getString("consumer.ftp.port").trim();
	final static String FTP_USER = rb.getString("consumer.ftp.user").trim();
	final static String FTP_PASSWORD = rb.getString("consumer.ftp.password").trim();
	final static String FTP_PROTOCOL = rb.getString("consumer.ftp.protocol").trim();
	final static String FTP_DIR = rb.getString("consumer.ftp.directoryName").trim();
	final static String FTP_OPTIONS = rb.getString("consumer.ftp.options").trim();

    public void configure() throws Exception {
    	if (!FTP_HOST.isEmpty()) {
			System.out.println("Trying to create the Camel route definition on the remote FTP server:" + FTP_HOST);
			String ftpUri = FTP_PROTOCOL+"://" + FTP_USER+"@"+FTP_HOST+":"+FTP_PORT+"/"+FTP_DIR+"?password="+FTP_PASSWORD;
			from(ftpUri+"&"+FTP_OPTIONS).to("file://"+DEST_FOLDER).log(LoggingLevel.INFO, "Processing ${id}");
			System.out.println("Camel FTP route initialized. Ready to receive Invoices from: " + ftpUri);
		}else {
			System.out.println("FTP Route not intialized. Check property 'ftp.host' in the configuration file");
		}	
    }
 

    /**
     * For test purpose only.
     * @return
     */
    public static Map<String, String> getPropertiesMap(){
    	Map<String, String> values = new HashMap<>();
    	values.put("DEST_FOLDER", DEST_FOLDER);
    	values.put("FTP_HOST", FTP_HOST);
    	values.put("FTP_PORT", FTP_PORT);
    	values.put("FTP_USER", FTP_USER);
    	values.put("FTP_PASSWORD", FTP_PASSWORD);
    	values.put("FTP_PROTOCOL", FTP_PROTOCOL);
    	values.put("FTP_DIR", FTP_DIR);   	
    	return values;
	}  
    
    
    
}
